import stardog

conn_details = {
  'endpoint': 'http://localhost:5820',
  'username': 'admin',
  'password': 'admin'
}

with stardog.Admin(**conn_details) as admin:
  db = admin.database('test')
  try:
    db.drop()
  except:
    pass


  options = { 'query.all.graphs': True, 'icv.enabled': True, 
  'icv.reasoning.enabled': True, 'transaction.isolation': 'SERIALIZABLE', 'reasoning.virtual.graph.enabled': True,
  'icv.consistency.automatic': True, 'reasoning.consistency.automatic': True, 'reasoning.schema.graphs': 'tag:stardog:api:context:default',
  'virtual.transparency': True }
  admin.new_database('test', options)
  
  #adds access to the virtual graphs in the default graph
  with stardog.Connection('test', **conn_details) as conn:
    results = conn.select('select * { graph  <virtual://advance_wars>{ ?a ?p ?o } }')
    print(results)
    bindings = results['results']['bindings']
    for binding in bindings:
        print(binding)
        print("")
