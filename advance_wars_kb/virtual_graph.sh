#!/bin/sh

/opt/stardog/bin/stardog-admin db create -n advance_wars
/opt/stardog/bin/stardog namespace add --prefix "" --uri "http://hamod.org/advance_wars/" advance_wars
/opt/stardog/bin/stardog-admin virtual add /var/opt/properties/advance_wars.properties /var/opt/properties/advance_wars_mappings.ttl