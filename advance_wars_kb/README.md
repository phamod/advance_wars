# Advance Wars KB
This is a stardog image creation script. This maps the parent folders image advance_wars_db postgres database to a stardog knowledge graph.

## creating the image
Run the following command from this directory to create the image
> docker build -t advance_wars_kb:latest .

## running the image
The command to run the image as a container is as follows.
> docker run -p 5820:5820 -v stardog-home:/var/opt/stardog -d --restart always --name stardog advance_wars_kb  

this will start a container that requires a stardog license. You can download a trial if connected or it can be added to the Dockerfile to copy the license file to /var/opt/stardog. ready to add a virtual graph mapped to the postgres database. after the container starts up run the following command to map virtual graph advance_wars.
> docker exec stardog bash -c "/virtual_graph.sh"  

To check the image has the virtual graph run the ontology.py script

