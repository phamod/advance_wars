CREATE TABLE advance_wars.unit(
    unit_id SERIAL PRIMARY KEY, 
    name VARCHAR(25) NOT NULL, 
    movement_speed INTEGER NOT NULL, 
    movement_type VARCHAR(10) NOT NULL, 
    vision INTEGER NOT NULL, 
    max_fuel INTEGER NOT NULL, 
    weapon_1 INTEGER, 
    weapon_2 INTEGER, 
    unit_type VARCHAR(10) NOT NULL,
    cost INTEGER);

CREATE TABLE advance_wars.weapon(
    weapon_id SERIAL PRIMARY KEY,
    name VARCHAR(25) NOT NULL,
    max_ammo INTEGER NOT NULL,
    min_range INTEGER NOT NULL,
    max_range INTEGER NOT NULL
);

CREATE TABLE advance_wars.weapon_unit(
    weapon_unit_id SERIAL PRIMARY KEY,
    weapon_id INTEGER NOT NULL,
    target VARCHAR(10) NOT NULL
);

CREATE TABLE advance_wars.map_panel(
    map_panel_id SERIAL PRIMARY KEY,
    name VARCHAR(10) NOT NULL,
    def INTEGER NOT NULL,
    funds INTEGER,
    inftry INTEGER,
    mech INTEGER,
    tires INTEGER,
    tread INTEGER,
    air INTEGER,
    oozium INTEGER,
    trans INTEGER,
    ships INTEGER
);

CREATE TABLE advance_wars.movement_type (
    movement_type_id SERIAL PRIMARY KEY,
    movement_type VARCHAR(6)
);

CREATE TABLE advance_wars.unit_type (
    unit_type_id SERIAL PRIMARY KEY,
    unit_type VARCHAR(6)
);

CREATE TABLE advance_wars.commanding_officer(
    commanding_officer_id SERIAL PRIMARY KEY,
    co_power VARCHAR(10),
    co_super_power VARCHAR(10)
);

ALTER TABLE advance_wars.unit ADD FOREIGN KEY(weapon_1) REFERENCES advance_wars.weapon(weapon_id);
ALTER TABLE advance_wars.unit ADD FOREIGN KEY(weapon_2) REFERENCES advance_wars.weapon(weapon_id);
ALTER TABLE advance_wars.weapon_unit ADD FOREIGN KEY(weapon_id) REFERENCES advance_wars.weapon(weapon_id);