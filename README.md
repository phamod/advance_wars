# Advance wars db
Simple database container to get the feel for creating tables. This is a set of data inspired by Intelligent Systems Advance Wars. 

## Creating the database image
To create the image with the database run the following command from within this directory.
> docker build -t advance_wars_db:latest .

## running the image
After the image is created run the following command to start up the container with the database hosted on a postgres database server.
> docker run -p 5432:5432 -e POSTGRES_PASSWORD=postgres -v advance-wars-data:/var/lib/postgresql/data advance_wars_db  

The default username and database are postgres. For more information see https://hub.docker.com/_/postgres/